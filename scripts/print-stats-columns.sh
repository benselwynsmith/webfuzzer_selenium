#!/usr/bin/env bash
if [ ! -e "./logs/stats.csv" ] ; then
  echo "Run from project root. ./scripts/print-stats-columns.sh  ./logs/stats.csv file must be present!"
  exit  -1
fi
if [ $# -eq 0 ]
  then
    echo "Usage: ./scripts/print-stats-columns.sh <column N_1>,<column N_2>,...,<column N_k>"
    echo "Available column headers:"
    COLUMNS=`head -1 ./logs/stats.csv`
    ITER=1
    for col in $COLUMNS
    do
      echo $ITER " " $col
      ((ITER++))
    done
    exit -1
fi
eval "cut -d $'\t' -f${1} ./logs/stats.csv"
