package nz.ac.vuw.httpfuzz.stan.pointsto;

import com.google.common.base.Preconditions;
import com.google.common.collect.Multimap;
import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Points to index.
 * @jens dietrich
 */
public class Pointsto {

    private Multimap<Variable, HObject> registry = null;

    public Pointsto(Supplier<Multimap<Variable, HObject>> registryFactory) {
        Multimap<Variable, HObject> reg = registryFactory.get();
        Preconditions.checkNotNull(reg);
        this.registry = reg;
    }


    @Nonnull
    public Collection<HObject> getObjects(Variable var) {
        Collection<HObject> objects = registry.get(var);
        return objects==null? Collections.emptySet():objects;
    }

    public boolean mayAlias(Variable var1,Variable var2) {
        Collection<HObject> objects1 = registry.get(var1);
        if (objects1.isEmpty()) {
            return false;
        }
        Collection<HObject> objects2 = registry.get(var2);
        if (objects2.isEmpty()) {
            return false;
        }
        Optional<HObject> objectInIntersection = objects1.parallelStream().filter(obj -> objects2.contains(obj)).findAny();
        return objectInIntersection.isPresent();
    }

    public Set<Variable> getVariables()  {
        return Collections.unmodifiableSet(registry.keySet());
    }

    public Collection<HObject> getObjects()  {
        return Collections.unmodifiableCollection(registry.values());
    }
}


