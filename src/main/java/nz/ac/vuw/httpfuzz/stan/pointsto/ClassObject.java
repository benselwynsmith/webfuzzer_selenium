package nz.ac.vuw.httpfuzz.stan.pointsto;

/**
 * Represents a class represented as an object.
 * @author jens dietrich
 */
public class ClassObject extends HObject {

    // must be initialised depending on the language used
    public static String TYPE_NAME = Class.class.getName();

    public String getType() {
        return TYPE_NAME;
    }

    private String value = null;

    public ClassObject(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassObject that = (ClassObject) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return "ClassObject{" +
                "value='" + value + '\'' +
                '}';
    }
}
