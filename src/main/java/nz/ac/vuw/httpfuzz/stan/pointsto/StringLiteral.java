package nz.ac.vuw.httpfuzz.stan.pointsto;

/**
 * String constant found in a program.
 * @author jens dietrich
 */
public class StringLiteral extends HObject {

    // must be initialised depending on the language used
    public static String TYPE_NAME = null;

    public String getType() {
        assert TYPE_NAME != null;
        return TYPE_NAME;
    }

    private String value = null;

    public StringLiteral(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StringLiteral that = (StringLiteral) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return "StringLiteral{" +
                "value='" + value + '\'' +
                '}';
    }
}
