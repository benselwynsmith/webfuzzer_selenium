package nz.ac.vuw.httpfuzz.stan.pointsto;

/**
 * Represents anything that cannot be associated with any other object kinds.
 * @author jens dietrich
 */
public class OtherObject extends HObject {

    // must be initialised depending on the language used
    public static String TYPE_NAME = null;

    public String getType() {
        assert TYPE_NAME != null;
        return TYPE_NAME;
    }

    private String value = null;

    public OtherObject(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OtherObject that = (OtherObject) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return "OtherObject{" +
                "value='" + value + '\'' +
                '}';
    }
}
