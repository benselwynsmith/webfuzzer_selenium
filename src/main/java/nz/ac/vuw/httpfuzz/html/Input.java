package nz.ac.vuw.httpfuzz.html;

import java.util.List;
import java.util.Objects;

/**
 * Representation of an html input element within a form.
 * @author jens dietrich
 */
public class Input {
    private String type = null;
    private String name = null;
    private String value = null;
    private List options = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List getOptions() {
        return options;
    }

    public void setOptions(List options) {
        this.options = options;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Input input = (Input) o;
        return Objects.equals(type, input.type) &&
                Objects.equals(name, input.name) &&
                Objects.equals(value, input.value) &&
                Objects.equals(options, input.options);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value, options);
    }

    @Override
    public String toString() {
        return "Input{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
