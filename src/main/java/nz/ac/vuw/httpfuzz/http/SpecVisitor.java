package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;

/**
 * Visitor to explore the structure of spec.
 * @author jens dietrich
 */
public interface SpecVisitor {

    default boolean visit (RequestSpec spec) {return true;}
    default boolean visit (HeadersSpec spec) {return true;}
    default boolean visit (HeaderSpec spec) {return true;}
    default boolean visit (ParametersSpec spec) {return true;}
    default boolean visit (ParameterSpec spec) {return true;}
    default boolean visit (URISpec spec) {return true;}
    default boolean visit (MethodSpec spec) {return true;}
    default boolean visit (ValueSpec spec) {return true;}

    default void endVisit (RequestSpec spec) {}
    default void endVisit (HeadersSpec spec) {}
    default void endVisit (HeaderSpec spec) {}
    default void endVisit (ParametersSpec spec) {}
    default void endVisit (ParameterSpec spec) {}
    default  void endVisit (URISpec spec) {}
    default void endVisit (MethodSpec spec) {}
    default void endVisit (ValueSpec spec) {}
}
