package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

/**
 * Produces lists of parameters specs.
 * @author jens dietrich
 */
public class ParametersSpecGenerator implements Generator<ParametersSpec> {

    private static ParameterSpecGenerator parameterGenerator = new ParameterSpecGenerator();

    public ParametersSpecGenerator() {
        super();
    }

    @Override
    public ParametersSpec apply(SourceOfRandomness sor, Context context) {

        ParametersSpec params = new ParametersSpec();
        int count = sor.getRandomPositiveNumberFromExponentialDistribution(2,7);
        for (int i=0;i<count;i++) {
            params.add(parameterGenerator.apply(sor,context));
        }
        params.add(SpecSource.RANDOM); // for random count
        return params;
    }
}
