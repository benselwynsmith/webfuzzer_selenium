package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.Set;

public class MethodSpecGenerator implements Generator<MethodSpec> {

    @Override
    public MethodSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        return sourceOfRandomness.choose(context.getProfile().getUsedMethods());
    }
}
