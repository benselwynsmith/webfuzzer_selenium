package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.function.Supplier;

/**
 * Produces request specs.
 * @author jens dietrich
 */
public class RequestSpecGenerator implements Generator<RequestSpec> {

    private RandomRequestSpecGenerator randomGenerator = new RandomRequestSpecGenerator();
    private RequestSpecMutator mutatingGenerator = new RequestSpecMutator();
    private RequestSpecCrossover crossoverGenerator = new RequestSpecCrossover();
    private RequestFromCapturedFormGenerator fromFormGenerator = new RequestFromCapturedFormGenerator();
    private EntryPointBasedRequestGenerator entryPointBasedRequestGenerator = new EntryPointBasedRequestGenerator();
    private InjectTaintMutator injectTaintMutator = new InjectTaintMutator();

    public RequestSpecGenerator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        try {
            int highScoringRequestCount = context.getDynamicModel().getTopRequestSpecs().size();

            int[] probabilities = new int[6]; // rel probabilities -- [RANDOM,MUTATE,CROSSBREED,FORM,ENTRY_POINT] -- will be scaled to 100

            int formCount = context.getDynamicModel().getForms().size() + context.getStaticModel().getForms().size();
            int entryPointCount = context.getStaticModel().getEntryPoints().size();
            int criticalMethodExecutionTriggersCount = context.getDynamicModel().getRequestSpecsReachingUnsafeMethods().size();

            probabilities[0] = 10;
            probabilities[1] = highScoringRequestCount>0 ? (highScoringRequestCount>10?10:5) : 0;
            probabilities[2] = highScoringRequestCount>10 ? 5 : 0;
            probabilities[3] = formCount>0 ? (formCount>5 ? 10 : 5) : 0;
            probabilities[4] = entryPointCount>0 ?  (entryPointCount>5 ? 10 : 5) : 0;
            probabilities[5] = criticalMethodExecutionTriggersCount>0 ? (criticalMethodExecutionTriggersCount>5 ? 10 : 5) : 0;

            probabilities = ScalingUtil.scaleTo100(probabilities);

            try {
                ProbabilisticSupplierFactory supplierFactory = ProbabilisticSupplierFactory.create()
                    .withProbability(probabilities[0]).createValue(() -> randomGenerator.apply(sourceOfRandomness, context))
                    .withProbability(probabilities[1]).createValue(() -> mutatingGenerator.apply(sourceOfRandomness, context))
                    .withProbability(probabilities[2]).createValue(() -> crossoverGenerator.apply(sourceOfRandomness, context))
                    .withProbability(probabilities[3]).createValue(() -> fromFormGenerator.apply(sourceOfRandomness, context))
                    .withProbability(probabilities[4]).createValue(() -> entryPointBasedRequestGenerator.apply(sourceOfRandomness, context))
                    .withProbability(probabilities[5]).createValue(() -> injectTaintMutator.apply(sourceOfRandomness, context))
                    ;

                Supplier<RequestSpec> supplier = supplierFactory.build();
                RequestSpec requestSpec = supplier.get();
                
                return requestSpec;
            } catch (Exception x) {
                x.printStackTrace();
            }
            return null;
        }
        catch (Exception x) {
            x.printStackTrace();
            return null;
        }
    }

}
