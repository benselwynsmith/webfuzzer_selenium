package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import static org.apache.commons.lang3.RandomStringUtils.*;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Produces key-value pair to be used as http headers.
 * @author jens dietrich
 */
public class HeaderSpecGenerator implements Generator<HeaderSpec> {

    private static Map<String,Supplier<String>> STANDARD_HEADERS = new HashMap<>();
    private static Supplier<String> randomString(int length) {
        return () -> randomAlphanumeric(length);
    };

    private static Supplier<String> randomDate() {
        return () -> "___42";//+ Math.abs(System.currentTimeMillis() - RandomUtils.nextLong());
    };

    static {
        STANDARD_HEADERS.put("A-IM",randomString(100));
        STANDARD_HEADERS.put("Accept",randomString(100));
        STANDARD_HEADERS.put("Accept-Charset",randomString(100));
        //STANDARD_HEADERS.put("Accept-Datetime",randomString(100));
        STANDARD_HEADERS.put("Accept-Encoding",randomString(100));
        STANDARD_HEADERS.put("Accept-Language",randomString(100));
        STANDARD_HEADERS.put("Access-Control-Request-Method",randomString(100));
        STANDARD_HEADERS.put("Access-Control-Request-Headers",randomString(100));
        STANDARD_HEADERS.put("Authorization",randomString(100));
        STANDARD_HEADERS.put("Cache-Control",randomString(100));
        STANDARD_HEADERS.put("Connection",randomString(100));
        //STANDARD_HEADERS.put("Content-Length",randomString(100)); set by client
        STANDARD_HEADERS.put("Content-MD5",randomString(100));
        STANDARD_HEADERS.put("Content-Type",randomString(100));
        STANDARD_HEADERS.put("Cookie",randomString(100));
        //STANDARD_HEADERS. put("Date",randomDate());   // -- @TODO must be date, otherwise this will create a jetty exception
        STANDARD_HEADERS.put("Expect",randomString(100));
        STANDARD_HEADERS.put("Forwarded",randomString(100));
        STANDARD_HEADERS.put("From",randomString(100));
        STANDARD_HEADERS.put("Host",randomString(100));
        STANDARD_HEADERS.put("HTTP2-Settings",randomString(100));
        STANDARD_HEADERS.put("If-Match",randomString(100));
        //STANDARD_HEADERS.put("If-Modified-Since",randomString(100));
        STANDARD_HEADERS.put("If-None-Match",randomString(100));
        STANDARD_HEADERS.put("If-Range",randomString(100));
        //STANDARD_HEADERS.put("If-Unmodified-Since",randomString(100));
        STANDARD_HEADERS.put("Max-Forwards",randomString(100));
        STANDARD_HEADERS.put("Origin",randomString(100));
        STANDARD_HEADERS.put("Pragma",randomString(100));
        STANDARD_HEADERS.put("Proxy-Authorization",randomString(100));
        STANDARD_HEADERS.put("Range",randomString(100));
        STANDARD_HEADERS.put("Referer",randomString(100));
        STANDARD_HEADERS.put("TE",randomString(100));
        STANDARD_HEADERS.put("User-Agent",randomString(100));
        STANDARD_HEADERS.put("Upgrade",randomString(100));
        STANDARD_HEADERS.put("Via",randomString(100));
        STANDARD_HEADERS.put("Warning",randomString(100));
    }

    public HeaderSpecGenerator() {
        super();
    }

    @Override
    public HeaderSpec apply(SourceOfRandomness sor, Context context) {
        Set<String> headerKeys = context.getStaticModel().getHeaderNames();
        Set<String> literals = context.getStaticModel().getStringLiterals(EnumSet.of(StaticModel.Scope.APPLICATION, StaticModel.Scope.LIBRARIES));

        Supplier<ValueSpec<String>> keys = headerKeys.size()>0 ?
            ProbabilisticSupplierFactory.create()
                .withProbability(35).createValue(() -> new ValueSpec<>(sor.choose(STANDARD_HEADERS.keySet()), EnumSet.of(SpecSource.FIXED)))
                .withProbability(35).createValue(() -> new ValueSpec<>(sor.choose(headerKeys), EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(10).createValue(() -> literals.isEmpty()?
                    new ValueSpec<>(randomAlphanumeric(50), EnumSet.of(SpecSource.RANDOM)) :
                    new ValueSpec<>(sanitise(LiteralSelector.choseLiteral(sor,context)), EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(10).createValue(() -> new ValueSpec<>(randomAlphanumeric(50),EnumSet.of(SpecSource.RANDOM))) // random string
                .withProbability(10).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .build() :
            ProbabilisticSupplierFactory.create()
                .withProbability(70).createValue(() -> new ValueSpec<>(sor.choose(STANDARD_HEADERS.keySet()),EnumSet.of(SpecSource.FIXED)))
                .withProbability(10).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .withProbability(10).createValue(() -> literals.isEmpty()?
                            new ValueSpec<>(randomAlphanumeric(50), EnumSet.of(SpecSource.STATIC_ANALYSIS)) :
                            new ValueSpec<>(sanitise(LiteralSelector.choseLiteral(sor,context)), EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(10).createValue(() -> new ValueSpec<>(randomAlphanumeric(50),EnumSet.of(SpecSource.RANDOM))) // random string
                .build() ;
        final ValueSpec<String> key = keys.get();

        Supplier<ValueSpec<String>> values = ProbabilisticSupplierFactory.create()
                .onCondition(() -> STANDARD_HEADERS.containsKey(key)).createValue(() -> STANDARD_HEADERS.get(key).get())
                .withProbability(35).createValue(() -> literals.isEmpty()?
                        new ValueSpec<>(randomAlphanumeric(50),EnumSet.of(SpecSource.RANDOM)) :
                        new ValueSpec<>(sanitise(LiteralSelector.choseLiteral(sor,context)),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(10).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .withProbability(25).createValue(() -> new ValueSpec<>(randomAlphanumeric(50),EnumSet.of(SpecSource.RANDOM))) // random string
                .withProbability(10).createValue(() -> new ValueSpec<>("" + sor.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                .withProbability(10).createValue(() -> new ValueSpec<>("" + -sor.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                .withProbability(10).createValue(() -> new ValueSpec<>("" +sor.nextDouble(-100,1000),EnumSet.of(SpecSource.RANDOM)))
                .build() ;
        final ValueSpec<String> value = values.get();

        HeaderSpec headerSpec = new HeaderSpec(key,value,SpecProvenanceUtils.merge(key.getSources(),value.getSources(),EnumSet.of(SpecSource.RANDOM)));

        return headerSpec;

    }

    public static Set<String> getStandardHeaderNames() {
        return STANDARD_HEADERS.keySet();
    }

    private String sanitise(String s) {
        return s.replaceAll("\\s*","");
    }
}
