package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;

import java.util.EnumSet;
import java.util.Objects;

/**
 * A simple string wrapper.
 * @author jens dietrich
 */
public class ValueSpec<T> implements Spec,VisitableSpec {

    private T value = null;
    private EnumSet<SpecSource> sources = null;

    public ValueSpec(T value, EnumSet<SpecSource> sources) {
        this.value = value;
        this.sources = sources;
    }

    @Override
    public void accept(SpecVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return sources;
    }

    public T getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueSpec<?> valueSpec = (ValueSpec<?>) o;
        return Objects.equals(value, valueSpec.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "ValueSpec{" +
                "value=" + value +
                '}';
    }
}
