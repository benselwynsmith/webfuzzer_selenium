package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.RandomUtils;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Generator for parameter values.
 * @author jens dietrich
 */
public class ParameterValueGenerator implements Generator<ValueSpec<String>> {

    @Override
    public ValueSpec<String> apply(SourceOfRandomness sor, Context context) {

        Set<String> literals = context.getStaticModel().getStringLiterals(EnumSet.of(StaticModel.Scope.APPLICATION, StaticModel.Scope.LIBRARIES));

        Supplier<ValueSpec<String>> supplier = ProbabilisticSupplierFactory.create()
            .withProbability(40).createValue(() -> new ValueSpec<>(sor.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
            .withProbability(20).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
            .withProbability(20).createValue(() -> new ValueSpec<>(randomAlphanumeric(RandomUtils.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
            .withProbability(10).createValue(() -> new ValueSpec<>("" + sor.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
            .withProbability(5).createValue(() -> new ValueSpec<>("" + -sor.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
            .withProbability(5).createValue(() -> new ValueSpec<>("" +sor.nextDouble(-100,1000),EnumSet.of(SpecSource.RANDOM)))
            .build() ;

        ValueSpec<String> value = supplier.get();

        // handle null -- in case one of the lists is empty !
        return value==null ? new ValueSpec<>(sor.defaultRandomString(),EnumSet.of(SpecSource.STATIC_ANALYSIS)): value;
    }
}
