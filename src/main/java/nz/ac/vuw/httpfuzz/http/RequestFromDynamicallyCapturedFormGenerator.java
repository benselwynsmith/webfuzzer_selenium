package nz.ac.vuw.httpfuzz.http;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import nz.ac.vuw.httpfuzz.html.Form;

import java.util.*;

/**
 * Generate a request from a form captured from feedback.
 * @author jens dietrich
 */
public class RequestFromDynamicallyCapturedFormGenerator extends AbstractRequestFromCapturedFormGenerator {

    // this is used to point to the origin of the form
    protected EnumSet<SpecSource> getProvenance() {
        return EnumSet.of(SpecSource.DYNAMIC_ANALYSIS);
    }

    protected Form getForm(SourceOfRandomness sourceOfRandomness, Context context) {
        Collection<Form> forms = context.getDynamicModel().getForms();
        // callers must ensure this
        Preconditions.checkState(!forms.isEmpty(),"No forms have been recorded");
        return sourceOfRandomness.choose(forms);
    }

}
