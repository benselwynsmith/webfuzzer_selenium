package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

/**
 * Produces lists of header specs.
 * @author jens dietrich
 */
public class HeadersSpecGenerator implements Generator<HeadersSpec> {

    private static HeaderSpecGenerator headerGenerator = new HeaderSpecGenerator();

    public HeadersSpecGenerator() {
        super();
    }

    @Override
    public HeadersSpec apply(SourceOfRandomness sor, Context context) {

        HeadersSpec headers = new HeadersSpec();
        int count = sor.getRandomPositiveNumberFromExponentialDistribution(2,7);
        for (int i=0;i<count;i++) {
            headers.add(headerGenerator.apply(sor,context));
        }
        headers.add(SpecSource.RANDOM); // for random count

        return headers;
    }
}
