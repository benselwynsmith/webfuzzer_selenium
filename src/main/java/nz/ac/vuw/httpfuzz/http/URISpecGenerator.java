package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Supplier;
import static org.apache.commons.lang3.RandomStringUtils.*;

/**
 * Generator for URIs.
 * @author jens dietrich
 */
public class URISpecGenerator implements Generator<URISpec> {

    @Override
    public URISpec apply(SourceOfRandomness sor, Context context) {


        Set<EntryPoint> entryPoints = context.getStaticModel().getEntryPoints();

        Supplier<ValueSpec<String>> pathSupplier = entryPoints!=null && entryPoints.size()>0 ?
                ProbabilisticSupplierFactory.create()
                        .withProbability(80).createValue(() -> new ValueSpec<>(sor.choose(entryPoints).getUrlPattern(), EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                        .withProbability(10).createValue(() -> new ValueSpec<>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                        .withProbability(10).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                        .build() :
                ProbabilisticSupplierFactory.create()
                        .withProbability(80).createValue(() -> new ValueSpec<>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                        .withProbability(20).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                        .build();

        URISpec uri = new URISpec();

        uri.setScheme(sor.choose(context.getProfile().getSchemes()));
        uri.setHost(context.getProfile().getHost());
        uri.setPort(context.getProfile().getPort());
        for (String pathElement:context.getProfile().getPathFirstTokens()) {
            uri.addToPath(new ValueSpec(pathElement,EnumSet.of(SpecSource.FIXED)));
        }

        // sor.getRandomPositiveNumberFromExponentialDistribution()
        int pathLength = sor.getRandomPositiveNumberFromExponentialDistribution(1.3,5);
        for (int i=0;i<pathLength;i++) {
            ValueSpec<String> pathSpec = pathSupplier.get();
            uri.addToPath(pathSpec);
        }

        uri.setProvenance(EnumSet.of(SpecSource.RANDOM));

        return uri;
    }


    static ValueSpec<String> randomURLString(SourceOfRandomness sor, Context context) {
        int length = sor.getRandomPositiveNumberFromExponentialDistribution(5,30);
        Supplier<ValueSpec<String>> str = ProbabilisticSupplierFactory.create()
            .withProbability(30).createValue(() -> new ValueSpec<>(LiteralSelector.choseLiteral(sor, context),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
            .withProbability(30).createValue(() -> new ValueSpec<>(randomAlphanumeric(length),EnumSet.of(SpecSource.STATIC_ANALYSIS))) // random string
            .withProbability(20).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
            .withProbability(20).createValue(() -> new ValueSpec<String>(""+sor.getRandomPositiveNumberFromParetoDistribution(100),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
            .build() ;
        return str.get();
    }
}
