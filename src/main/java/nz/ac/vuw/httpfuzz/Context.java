package nz.ac.vuw.httpfuzz;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.log4j.Logger;

/**
 * A singleton describing the project top be fuzzed, and giving generators
 * access to static preanaysis and dynamic feedback.
 * The singleton instance is accessed by the generators used by the quickcheck-based fuzzer, so
 * it must be initialised before fuzzing starts.
 * @author jens dietrich
 */
public class Context<SPEC extends Spec, SCORE extends Comparable<SCORE>, XPOINT extends ExecutionPoint> {

    public static final Logger LOGGER = LogSystem.getLogger("context");

    protected static Context DEFAULT = null;

    public static Context getDefault() {
        Preconditions.checkNotNull(DEFAULT, "Context not initialised !");
        return DEFAULT;
    }

    public void install() {
        DEFAULT = this;
        LOGGER.info("Context initialised !");
    }

    private Profile profile = null;
    private StaticModel staticModel = null;
    private DynamicModel dynamicModel = null;

    public Context(Profile profile, StaticModel staticModel, DynamicModel dynamicModel) {
        this.profile = profile;
        this.staticModel = staticModel;
        this.dynamicModel = dynamicModel;
    }

    public Profile getProfile() {
        return profile;
    }

    public StaticModel getStaticModel() {
        return staticModel;
    }

    public DynamicModel getDynamicModel() {
        return dynamicModel;
    }

}
