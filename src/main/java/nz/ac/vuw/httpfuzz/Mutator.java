package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import org.checkerframework.checker.units.qual.A;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Mutates objects.
 * The mutation result should be a different object from the input.
 * @author jens dietrich
 */
@FunctionalInterface
public interface Mutator<T extends Spec>  {
    T apply(SourceOfRandomness sourceOfRandomness, Context context, T original);

    // TODO: implement default andThen
}
