package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.MethodSpec;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * The model exposes information from static analysis that can be used to direct fuzzing.
 * @author jens dietrich
 */
public interface StaticModel {

   // levels can be used as qualifiers when extracting constants
   // this can be used to model priorities and context
   enum Scope {
       APPLICATION, LIBRARIES, SYSTEM
   }

   /**
    * Implementation that can be used if no real static model is used.
    */
   public static StaticModel NULL = new StaticModel() {
      @Override
      public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getEntryPointsURLPatterns() {
         return Collections.emptySet();
      }

      @Override
      public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getRequestParameterNames() {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getHeaderNames() {
         return Collections.emptySet();
      }

      @Override
      public Set<EntryPoint> getEntryPoints() {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<MethodSpec> getMethods() {
         return Collections.emptySet();
      }

      @Override
      public Set<Form> getForms() {
         return Collections.emptySet();
      }
   };

   /**
    * Extracts string literals.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getStringLiterals(EnumSet<Scope> scopes);

   /**
    * Extracts names of classes and names that can be used for reflection.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getReflectiveNames(EnumSet<Scope> scopes);

   /**
    * Extracts int literals.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<Integer> getIntLiterals(EnumSet<Scope> scopes);

   /**
    * Extracts (possible) request parameter names.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getRequestParameterNames() ;

   /**
    * Extracts (possible) header names.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getHeaderNames() ;

   /**
    * Extracts entry points (URLs).
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<EntryPoint> getEntryPoints() ;

   /**
    * Get the URL patterns extracted from the entry points.
    * @return
    */
   Set<String> getEntryPointsURLPatterns() ;

   /**
    * Extracts supported method specs.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<MethodSpec> getMethods();

   /**
    * Extract forms from static html pages, and potenially also html templates.
    * @return
    */
   Set<Form> getForms();


}
