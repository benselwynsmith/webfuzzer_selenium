package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import java.util.function.BiFunction;

/**
 * Generator for random objects.
 * @author jens dietrich
 */
public interface Generator<T>  extends BiFunction<SourceOfRandomness,Context,T> {
}
