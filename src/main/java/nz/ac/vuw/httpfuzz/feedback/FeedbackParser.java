package nz.ac.vuw.httpfuzz.feedback;

import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.jee.war.MethodInvocation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;

/**
 * Simple feedback parser.
 * @author jens dietrich
 */
public class FeedbackParser {

    public static Set<ExecutionPoint> extractMethodSpecs(JSONObject json) {
        try {
            JSONArray arr = json.getJSONArray("invokedMethods");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<ExecutionPoint> specs = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                String line = arr.getString(i);
                String[] parts = line.split("::");
                assert parts.length == 3;
                ExecutionPoint spec = new MethodInvocation(parts[0], parts[1], parts[2]);
                specs.add(spec);
            }
            return specs;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }

    public static Set<String> extractRequestParameterNames(JSONObject json) {
        return convertFlatJSONArray(json, "requestParameterNames");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    public static Set<String> extractStackTracesForUnsafeMethods(JSONObject json) {
        return convertFlatJSONArray(json,"unsafeSinkInvocationStackTraces");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    private static Set<String> convertFlatJSONArray(JSONObject json,String key)  {
        try {
            JSONArray arr = json.getJSONArray(key);  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<String> values = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                values.add(""+arr.get(i));
            }
            return values;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }

    
     /**
     * @shawn
     * @param json
     * @return
     */
    public static Set<TaintFlowSpec> extractTaintflows(JSONObject json) {
    
       try {
            JSONArray taintFlowsData = json.getJSONArray("taintFlows");
            Set<TaintFlowSpec> taintFlows = new HashSet<>();
            for (int i = 0; i < taintFlowsData.length(); i++) {
                taintFlows.add(extractTaintFlowSpec(taintFlowsData.getString(i)));
            }
            return taintFlows;
        } catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }
    /**
     * @shawn
     * @return a value of type TaintFlowSpec which contains the sink/unsafe method name, list of tainted string arguments
     * and string of \n joined provenance strings as it appears in feedback
     */
    private static TaintFlowSpec extractTaintFlowSpec(String s) {
        String sinkName = "";
        List<String> sourceStrings = new ArrayList<>();
        String[] p = s.split("\n");
        for(int i = 0; i < p.length; i++) {
            String item = p[i];
            String[] parts = item.split(";");
            for(int j = 0; j < parts.length; j++) {
                if (parts[j].startsWith("string")) {
                    sourceStrings.add(parts[j].substring(7));
                }
                if (parts[j].startsWith("sink")) {
                    sinkName = parts[j].substring(5);
                }
            }
        }
        return new TaintFlowSpec(sinkName, sourceStrings, s);
    }
}
