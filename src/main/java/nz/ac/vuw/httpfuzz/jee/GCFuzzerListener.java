package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.http.RequestSpec;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Listener that periodically triggers garbage collection.
 * This should not be necessary, but see https://bitbucket.org/jensdietrich/webfuzzer/issues/19/potenial-memory-leak-when-fuzzing-shopizer
 * @author jens dietrich
 */
public class GCFuzzerListener extends AbstractFuzzerListener {
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void requestSent(RequestSpec request, int statusCode) {
        super.requestSent(request, statusCode);
        if (counter.incrementAndGet()%1_000==0) {
            System.gc();
        }
    }
}
