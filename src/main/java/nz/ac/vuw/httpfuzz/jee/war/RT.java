package nz.ac.vuw.httpfuzz.jee.war;

/**
 * Support to locate runtime artifacts.
 * @author jens dietrich
 */
public class RT {
    static boolean isFuzzerRuntimeClass(String name) {
        return name.contains("nz/ac/vuw/httpfuzz/jee/") || name.contains("InvocationTracking");
    }
}
