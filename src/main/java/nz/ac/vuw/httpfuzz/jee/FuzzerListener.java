package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.feedback.TaintFlowSpec;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.http.HttpResponse;

import java.util.Set;

/**
 * Interface for fuzzer listeners. Can be used for logging, exporting results, statistics etc.
 * @author jens dietrich
 */
public interface FuzzerListener {

    void fuzzingStarts();

    void fuzzingEnds();

    // requests rejected by the fuzzer, not sent
    void requestRejected(RequestSpec request);

    void requestSent(RequestSpec request, int statusCode);

    void feedbackRequestSent(String ticketId, int statusCode);

    void newExecutionPointsDiscovered (RequestSpec requestSpec, Set<ExecutionPoint> executionPoints);

    void newRequestParameterNamesDiscovered (RequestSpec requestSpec, Set<String> requestParameterNames);

    void newInvocationsOfCriticalMethodDiscovered (RequestSpec requestSpec, String stackTrace);

    void newXSSDiscovered (RequestSpec requestSpec, String response, String token);

    void newFormsDiscovered(RequestSpec requestSpec,Set<Form> forms);

    void newTaintFlows(RequestSpec originalRequestSpec, TaintFlowSpec taintFlow, HttpResponse originalResponse);

}
