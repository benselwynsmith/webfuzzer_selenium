package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.RTConstants;
import nz.ac.vuw.httpfuzz.feedback.TaintFlowSpec;
import nz.ac.vuw.httpfuzz.http.HeaderSpec;
import nz.ac.vuw.httpfuzz.http.ParameterSpec;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import nz.ac.vuw.httpfuzz.http.SpecProvenanceUtils;
import nz.ac.vuw.httpfuzz.jee.AbstractFuzzerListener;
import nz.ac.vuw.httpfuzz.jee.Loggers;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Exports detected taint flows to sensitive methods to json.
 * adapted from XSSExportFuzzerListener
 * @author shawn
 */
public class TaintFlowExportFuzzerListener extends AbstractFuzzerListener  {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private static final File folder = new File("logs");

    @Override
    public void fuzzingStarts() {
        super.fuzzingStarts();
        this.checkLogFolder(folder);
    }

    @Override
    public void newTaintFlows(RequestSpec requestSpec, TaintFlowSpec taintFlow, HttpResponse response) {
        super.newTaintFlows(requestSpec, taintFlow, response);

        File file = new File(folder,"taint-" + counter.incrementAndGet() + ".json");
        JSONObject json = new JSONObject();


        try (FileWriter out = new FileWriter(file)) {

            // request
            JSONObject jRequest = new JSONObject();
            jRequest.put("method",requestSpec.getMethodSpec().name());
            jRequest.put("uri",requestSpec.getUriSpec().toURI());
            Map<String,String> params = new HashMap<>();
            for (ParameterSpec paramSpec:requestSpec.getParametersSpec().getParams()) {
                params.put(paramSpec.getKey().getValue(),paramSpec.getValue().getValue());
            }
            jRequest.put("parameters",params);
            Map<String,String> headers = new HashMap<>();
            for (HeaderSpec headerSpec:requestSpec.getHeadersSpec().getHeaders()) {
                headers.put(headerSpec.getKey().getValue(),headerSpec.getValue().getValue());
            }
            jRequest.put("headers",headers);
            json.put("request",jRequest);

            Set<String> taintedRequestParts = SpecProvenanceUtils.extractTaintedParts(requestSpec).keySet();
            String taintedValues = "N/A";
            if (!taintedRequestParts.isEmpty()) {
                 taintedValues = taintedRequestParts.stream().collect(Collectors.joining(","));
            }

            jRequest.put("tainted-values", taintedValues);

            // response
            JSONObject jResponse = new JSONObject();
            String responseContent = "N/A";
            /* FIXME: Response is closed. Cache this in the record object?
            try {
                responseContent = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                Loggers.FUZZER.debug("IOException getting response for logging taint flow for request:" + requestSpec);
            }
            */
            jResponse.put("entity", responseContent);
            json.put("response", jResponse);

            json.put("tainted-input-provenance", taintFlow.provenance);

            json.write(out,1,3);
        } catch (Exception x) {
            Loggers.FUZZER.error("Error exporting details for request with taint flows", x);
        }
    }
}
