package nz.ac.vuw.httpfuzz.jee;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.DefaultDynamicModel;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Level;
import java.io.File;
import java.net.URI;
import java.util.*;
import java.util.function.Predicate;

/**
 * Adhoc fuzzing for webgoat 8.1.
 * TODO: to be abstracted for spring boot applications.
 * @author jens dietrich
 */
public class FuzzWebGoat81 extends AbstractFuzzer {

    // username / password must be setup manually by launching webgoat and configuring user in
    // localhost:8080/WebGoat/login
    public static final String USERNAME = "scottscott";
    public static final String PASSWORD = "tigertiger";

    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    private static File webgoatInstrumentedJar = new File("examples/webgoat/webgoat-server-v8.1.0.jar");

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(webgoatInstrumentedJar.exists());

        String[] args2 = {
                "-authAddress","/login",
                "-authParams","username=jensjens,password=jensjens",
                "-httpKeepAlive","true"
        };

        Options options = new Options()
            .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
            .addOption("trials", true, "The max number of generated requests (optional, default is " + DEFAULT_TRIALS + ")")
            .addOption("authAddress", true, "URL path for login request. E.g. '/login'")
            .addOption("authParams", true, "Comma separated list of key value pairs for login request. E.g. 'key1=value1,key2=value2...'")
            .addOption("randomseed", true, "Control randomness for the fuzzing run")
            .addOption("httpKeepAlive", true, "Set http client to use persistent connections");

        CommandLine cmd = new DefaultParser().parse(options, args2);

        FuzzWebGoat81 fuzzer = new FuzzWebGoat81();
        fuzzer.fuzz(cmd);
    }

    @Override
    protected void authenticate(CommandLine cmd) throws Exception {
        // disable inherited mechanism
        // TODO refactor -- order of steps deploy vs authenticate not right
    }



    protected void authenticate2() throws Exception {
        Loggers.FUZZER.info("Authenticate");
        List<BasicNameValuePair> authParams = new ArrayList<BasicNameValuePair>();
        authParams.add(new BasicNameValuePair("username", USERNAME));
        authParams.add(new BasicNameValuePair("password", PASSWORD));
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("http")
                .setHost("localhost")
                .setPort(8080)
                .setPath("WebGoat/login");

        URI uri = uriBuilder.build();
        HttpPost httpPost = new HttpPost(uri);

        //Add Parameters to request and send
        httpPost.setEntity(new UrlEncodedFormEntity(authParams, "UTF-8"));
        HttpResponse response = sendRequest(httpPost,5000,30);

        if (response!=null) {
            if (!this.isPostSuccessfull(response,302,location -> location.endsWith("welcome.mvc"))) {
                Loggers.FUZZER.info("Try to create user account " + USERNAME + " / " + PASSWORD);
                uriBuilder = new URIBuilder()
                        .setScheme("http")
                        .setHost("localhost")
                        .setPort(8080)
                        .setPath("WebGoat/register.mvc");
                uri = uriBuilder.build();
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", USERNAME));
                params.add(new BasicNameValuePair("password", PASSWORD));
                params.add(new BasicNameValuePair("matchingPassword", PASSWORD));
                params.add(new BasicNameValuePair("agree", "agree"));
                HttpPost registrationRequest = new HttpPost(uri);
                registrationRequest.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse registrationResponse = sendRequest(registrationRequest,1000,3);

                if (isPostSuccessfull(registrationResponse,302,location -> location.endsWith("attack"))) {
                    response = sendRequest(httpPost,5000,30);
                    if (!this.isPostSuccessfull(response,302,location -> location.endsWith("welcome.mvc"))) {
                        Loggers.FUZZER.fatal("Authentication has failed");
                        System.exit(1);
                    }
                }
                else {
                    Loggers.FUZZER.fatal("Authentication has failed");
                    System.exit(1);
                }

            }

            // access this service to initilise session
            // http://localhost:8080/WebGoat/WebGoatIntroduction.lesson.lesson
            uriBuilder = new URIBuilder()
                    .setScheme("http")
                    .setHost("localhost")
                    .setPort(8080)
                    .setPath("WebGoat/WebGoatIntroduction.lesson.lesson");
            uri = uriBuilder.build();
            HttpGet getRequest = new HttpGet(uri);
            Loggers.FUZZER.info("Accessing lesson to initialise session");
            response = sendRequest(getRequest,1000,3);
            if (response!=null && response.getStatusLine().getStatusCode()==200) {
                Loggers.FUZZER.info("Lesson initialised");
            }
            else {
                Loggers.FUZZER.fatal("Authentication has failed");
                System.exit(1);
            }
        }
        else {
            // exit
            Loggers.FUZZER.fatal("Authentication has failed");
            System.exit(1);
        }



    }

    private boolean isPostSuccessfull (HttpResponse response, int expectedStatusCode, Predicate<String> expectedLocationHeader) {
        int statuscode = response.getStatusLine().getStatusCode();

        // success looks as follows:
        // response code is 302
        // a JSESSIONID cookie is send for /WebGoat
        // the Location header is WebGoat/welcome.mvc   -- http://localhost:8080/WebGoat/welcome.mvc
        // on error, the locaction header is WebGoat/login?error -- http://localhost:8080/WebGoat/login?error

        if (statuscode==expectedStatusCode) {
            Header[] locationHeaders = response.getHeaders("Location");
            if (locationHeaders==null || locationHeaders.length==0) {
                Loggers.FUZZER.info("No Location header returned indicating success");
                return false;
            }
            else {
                if (locationHeaders.length>1) {
                    Loggers.FUZZER.warn("More than one Location header returned from login request !");
                }
                Header locationHeader = locationHeaders[0];
                if (expectedLocationHeader.test(locationHeader.getValue())) {
                    Loggers.FUZZER.info("Post successful");
                    return true;
                }
                else {
                    Loggers.FUZZER.warn("Login request returned location header " + locationHeader.getValue());
                    return false;
                }
            }
        }
        else {
            Loggers.FUZZER.info("Post has returned unexpected result, status is " + response.getStatusLine());
            return false;
        }
    }

    @Override
    protected void init(CommandLine cmd) throws Exception {
        super.init(cmd);

        Profile profile = new Profile.WebApplicationProfile("localhost",8080,"WebGoat");
        Context context = new Context(
                profile,
                new JEEStaticModel(webgoatInstrumentedJar,s->true,profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.install(); // will be referenced by generators as singleton

    }

    @Override
    protected void deploy(CommandLine cmd) throws Exception {

        File aspectjWeaverJar = new File("tools/aspectjweaver-1.9.5.jar");
        Preconditions.checkState(aspectjWeaverJar.exists());

        Process process = new ProcessBuilder()
            .command(
                "java",
                "-javaagent:"+aspectjWeaverJar.getAbsolutePath(),
                "-Daj.weaving.verbose=true",
                "-jar",webgoatInstrumentedJar.getAbsolutePath()
            )
            .start();

        ServerProcessOutput2Log.redirectOutAndError(process,Level.DEBUG,Level.DEBUG);

        Thread.sleep(10_000); // server needs some time to start !

        // TODO: need to wait for magic line containing
        //  "o.s.web.servlet.DispatcherServlet        : Completed initialization in 36 ms"
        startTime = System.currentTimeMillis();

        authenticate2();
    }

    @Override
    protected void undeploy(CommandLine cmd) throws Exception {
        // nothiong to do here
    }

    @Override
    protected void tearDown() throws Exception {
        Context context = Context.getDefault();
        Collection<RequestSpec> topRequests = context.getDynamicModel().getTopRequestSpecs();
        Map<RequestSpec, Object> targets = context.getDynamicModel().getTargets();

        // some reporting
        long runtime = System.currentTimeMillis() - startTime;
        assert runtime>0;
        Loggers.FUZZER.info("Tried " + trials + " requests in " + runtime + " ms");
        Loggers.FUZZER.info("" + topRequests.size() + " top requests registered");
        Loggers.FUZZER.info("" + targets.size() + " targets registered");

        Loggers.FUZZER.info("TARGET DETAILS:");
        for (RequestSpec req:targets.keySet()) {
            Loggers.FUZZER.info("\t" + req.getUriSpec().toURI() + " -> " + targets.get(req));
        }

        Loggers.FUZZER.info("TOP REQUEST DETAILS:");
        for (RequestSpec req:topRequests) {
            Loggers.FUZZER.info("\t" + req.getUriSpec().toURI());
        }
    }

}
