package nz.ac.vuw.httpfuzz.jee.war;

/**
 * Abstraction that models the scope of different classes in a zip.
 * This can be used to prioritise input generation.
 * @author jens dietrich
 */
public interface WarScopeModel {
    /**
     * Whether this is an application class.
     * @param outerName -- the name of a zip entry in a war, or the relative path in an web application
     * @param innerName -- the name of a zip entry within a jar within a war or web application, or null if the class file is not inside a jar
     * @return
     */
    boolean isApplicationClass(String outerName, String innerName);
    /**
     * Whether this is an application class.
     * @param outerName -- the name of a zip entry in a war, or the relative path in an web application
     * @param innerName -- the name of a zip entry within a jar within a war or web application, or null if the class file is not inside a jar
     * @return
     */
    boolean isLibClass(String outerName, String innerName);

    default boolean isFuzzerRuntimeClass (String outerName,String innerName) {
        return RT.isFuzzerRuntimeClass(innerName);
    }

}
