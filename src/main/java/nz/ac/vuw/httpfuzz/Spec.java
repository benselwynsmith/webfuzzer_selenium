package nz.ac.vuw.httpfuzz;

import java.util.EnumSet;

/**
 * The input specification representing randomised input.
 * @author jens dietrich
 */
public interface Spec {
    EnumSet<SpecSource> getSources();
}
