package nz.ac.vuw.httpfuzz.commons;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.ParetoDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Random;

/**
 * Functionality related to randomness, similar to com.pholser.junit.quickcheck.random.SourceOfRandomness
 * @author jens dietrich
 */
public class SourceOfRandomness {
    private final Random delegate = new Random();
    private long seed;

    public String defaultRandomString() {
        return RandomStringUtils.randomAlphanumeric(20);
    }

    public enum Type {
        CHARACTER("c"),
        INTEGRAL("d"),
        FLOAT("f"),
        STRING("s");

        private final String pattern;

        Type(String pattern) {
            this.pattern = pattern;
        }
    }

    public static <T extends Comparable<? super T>> int checkRange(
            Type type,
            T min,
            T max) {

        int comparison = min.compareTo(max);
        if (comparison > 0) {
            throw new IllegalArgumentException("bad range");
        }

        return comparison;
    }

    public boolean trueAt (int percentage) {
        Preconditions.checkArgument(percentage>0);
        Preconditions.checkArgument(percentage<100);
        int v = nextInt(100);
        return v < percentage;
    }


    public boolean trueAt (double ratio) {
        Preconditions.checkArgument(ratio>=0D);
        Preconditions.checkArgument(ratio<=1D);
        double v = nextDouble();
        return v < ratio;
    }

    // for selection based on sizes of two selections
    // true - select first, false - select second
    public boolean trueBasedOnSizeRatio (Collection items1,Collection items2) {
        int s1 = items1.size();
        int s2 = items2.size();
        Preconditions.checkArgument(s1>0 || s2>0);
        if (s1==0) {
            return false;
        }
        if (s2==0) {
            return true;
        }

        double ratio = ((double)s1)/(((double)s1) + ((double)s2));
        return trueAt(ratio);
    }

    public <T> T choose(Collection<T> items) {
        Object[] array = items.toArray(new Object[items.size()]);
        return array.length==0?null:(T) array[this.nextInt(array.length)];
    }

    public <T> T choose(T[] items) {
        return items[this.nextInt(items.length)];
    }

    public boolean nextBoolean() {
        return this.delegate.nextBoolean();
    }

    public void nextBytes(byte[] bytes) {
        this.delegate.nextBytes(bytes);
    }

    public byte[] nextBytes(int count) {
        byte[] buffer = new byte[count];
        this.delegate.nextBytes(buffer);
        return buffer;
    }

    public double nextDouble() {
        return this.delegate.nextDouble();
    }

    public float nextFloat() {
        return this.delegate.nextFloat();
    }

    public double nextGaussian() {
        return this.delegate.nextGaussian();
    }

    public int nextInt() {
        return this.delegate.nextInt();
    }

    public int nextInt(int n) {
        return this.delegate.nextInt(n);
    }

    public long nextLong() {
        return this.delegate.nextLong();
    }

    public void setSeed(long seed) {
        this.seed = seed;
        this.delegate.setSeed(seed);
    }

    public long seed() {
        return this.seed;
    }

    public byte nextByte(byte min, byte max) {
        return (byte)((int)this.nextLong((long)min, (long)max));
    }

    public char nextChar(char min, char max) {
        checkRange(Type.CHARACTER, min, max);
        return (char)((int)this.nextLong((long)min, (long)max));
    }

    public double nextDouble(double min, double max) {
        int comparison = checkRange(Type.FLOAT, min, max);
        return comparison == 0 ? min : min + (max - min) * this.nextDouble();
    }

    public float nextFloat(float min, float max) {
        int comparison = checkRange(Type.FLOAT, min, max);
        return comparison == 0 ? min : min + (max - min) * this.nextFloat();
    }

    public int nextInt(int min, int max) {
        return (int)this.nextLong((long)min, (long)max);
    }

    public long nextLong(long min, long max) {
        int comparison = checkRange(Type.INTEGRAL, min, max);
        return comparison == 0 ? min : choose(min, max);
    }

    public short nextShort(short min, short max) {
        return (short)((int)this.nextLong((long)min, (long)max));
    }

    public long choose(long min, long max) {
        checkRange(Type.INTEGRAL, min, max);

        /* There are some edges cases with integer overflows, for instance,
           when (max - min) exceeds Long.MAX_VALUE. These cases should be
           relatively rare under the assumption that choosing
           [Long.MIN_VALUE, Long.MAX_VALUE] can be simplified to choosing any
           random long. Thus, the optimization here only deals with the common
           situation that no overflows are possible (maybe the heuristic to
           detect that could be improved).
         */
        boolean noOverflowIssues =
                max < ((long) 1 << 62) && min > -(((long) 1) << 62);

        if (noOverflowIssues) {
            // fast path: use long computations
            long range = (max - min) + 1;
            long mask = findNextPowerOfTwoLong(range) - 1;

            // loop to avoid distribution bias (as would be the case
            // with modulo division)
            long generated;
            do {
                generated = Math.abs(nextLong()) & mask;
            } while (generated >= range);

            return generated + min;
        } else {
            // slow path: fall back to BigInteger to avoid any surprises
            return choose(
                    BigInteger.valueOf(min),
                    BigInteger.valueOf(max))
                    .longValue();
        }
    }

    static long findNextPowerOfTwoLong(long positiveLong) {
        return isPowerOfTwoLong(positiveLong)
                ? positiveLong
                : ((long) 1) << (64 - Long.numberOfLeadingZeros(positiveLong));
    }

    private static boolean isPowerOfTwoLong(long positiveLong) {
        return (positiveLong & (positiveLong - 1)) == 0;
    }

    public BigInteger nextBigInteger(int numberOfBits) {
        return new BigInteger(numberOfBits, this.delegate);
    }

    public BigInteger choose(
            BigInteger min,
            BigInteger max) {

        BigInteger range = max.subtract(min).add(BigInteger.ONE);
        BigInteger generated;

        do {
            generated = nextBigInteger(range.bitLength());
        } while (generated.compareTo(range) >= 0);

        return generated.add(min);
    }

    /**
     * Get a random positive number from an exponential distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param mean
     * @param max
     * @return
     */
    public int getRandomPositiveNumberFromExponentialDistribution(double mean,int max) {
        RealDistribution distribution = new ExponentialDistribution(mean);
        double dvalue = distribution.sample();
        int value = (int)Math.round(dvalue);
        if (value>max) {
            // try again -- careful with recursion at low max values !
            return getRandomPositiveNumberFromExponentialDistribution(mean,max);
        }
        else {
            return value;
        }
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param paretoIndex -- a value between 0 and 1, closer to 1 means the curve is steeper
     * @param max
     * @return
     */
    public int getRandomPositiveNumberFromParetoDistribution(double paretoIndex,int max) {
        RealDistribution distribution = new ParetoDistribution(1, paretoIndex);
        double dvalue = distribution.sample();
        int value = (int)Math.round(dvalue);
        if (value>max || value<1) {
            // try again -- careful with recursion at low max values !
            return getRandomPositiveNumberFromParetoDistribution(paretoIndex, max);
        }
        else {
            return value-1; // to include 0 !
        }
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param max
     * @return
     */
    public int getRandomPositiveNumberFromParetoDistribution(int max) {
        return getRandomPositiveNumberFromParetoDistribution(0.95D,max);
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * @return
     */
    public int getRandomPositiveNumberFromParetoDistribution() {
        return getRandomPositiveNumberFromParetoDistribution(0.95D,Integer.MAX_VALUE);
    }

}
