/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.vuw.httpfuzz.commons;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Several reusable IO related utilities.
 * @author jens dietrich
 */
public class IOUtils {

    public static IOUtils DEFAULT = new IOUtils();

    // if true, proper tmp files are used that will be deleted on exit
    // set to false for debugging, then tmp files will be greated in .tmp and not deleted to faciliate inspection

    public static boolean USE_TMP_FILES = true;
    public static String TMP = ".tmp";

    protected static Logger LOGGER = LogSystem.getLogger(IOUtils.class);

	public Collection<String[]> parse(File csv,int columnCount) throws Exception {
		return parse(csv,columnCount,"\t");
	}

    /**
     * Get a reader to access the content of a file. The reason to have this abstraction is
     * to cache file contents in memory in order to speed up experiments that must build datastructures
     * from the content of the same file again and again.
     * @param csv
     * @return
     * @throws Exception
     */
    public Reader getReader(File csv) throws Exception {
        return new FileReader(csv);
    }

	public Collection<String[]> parse(File csv,int columnCount,String separator) throws Exception {

        String line= null;
		Collection<String[]> records = new ArrayList<>();
        if (!csv.exists()) {
            LOGGER.warn("File not found - will assume that there are no records): " + csv.getAbsolutePath());
            return records;
        }
		try (BufferedReader reader = new BufferedReader(getReader(csv))) {
			while ((line = reader.readLine()) != null) {
                // expensive check
                // Preconditions.checkArgument(StringUtils.countMatches(line,separator)==columnCount-1,"Unexpected number of columns in CSV line, expected " + columnCount + " but found " + (StringUtils.countMatches(line,separator)+1));
				String[] nextRecord = parseCSVLine(line, separator, false);
				records.add(nextRecord);
			}
		}
	 	return records;
	}

	/**
	 * Fast-parse a csv line (avoid String.split as it has to compile a regex), tokens are in double quotes.
	 * @param line
	 * @param separator the separator string
	 * @param checkForComments if true, check whether lines start with # and if so, ignore them
	 * @return
	 */
	public static String[] parseCSVLine(String line,String separator,boolean checkForComments) {
		if (checkForComments && line.startsWith("#")) return null;
        return line.split(separator);
	}

    /**
     * Install a singleton instance.
     */
    public void install() {
        LOGGER.warn("IOUtils changed to: " + this);
        DEFAULT = this;
    }

    public File createTmpDir(String name) {
        File dir = new File(TMP);
        dir.mkdirs();
        dir = new File(dir,name);
        if (dir.exists()) {
            try {
                FileUtils.deleteDirectory(dir);
            }
            catch (IOException x) {
                LOGGER.error("Error deleting existing tmp folder " + dir.getAbsolutePath(),x);
            }
        }
        dir.mkdir();
        // if (USE_TMP_FILES) dir.deleteOnExit();
        return dir;
    }

    public File getTmpDir(String name) {
        File dir = new File(TMP);
        dir = new File(dir,name);
        Preconditions.checkArgument(dir.exists(),"Tmp directory does not exist: " + dir.getAbsolutePath());
        return dir;
    }


    public boolean releaseTmpDir(File dir) {
        return USE_TMP_FILES?dir.delete():true;
    }


    public static void zip(File zip,Collection<File> input,Function<File,String> naming) throws IOException {

        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip))) {
            for (File f:input) {
                ZipEntry e = new ZipEntry(naming.apply(f));
                out.putNextEntry(e);
                try (InputStream in = new FileInputStream(f)) {
                    org.apache.commons.io.IOUtils.copy(in,out);
                    out.closeEntry();
                }
            }
        }
    }

    public static void zip(File zip,File[] input,Function<File,String> naming) throws IOException {
        List<File> list = new ArrayList<>();
        for (File f:input) list.add(f);
        zip(zip,list,naming);
    }


    public static List<File> getJars(String path) {
        return getJars(path,",");
    }
    public static List<File> getJars(String path,String separator) {
        List<File> jars = new ArrayList<>();
        for (String s:path.split(separator)) {
            File f = new File(s);
            if (!f.exists()) {
                LOGGER.warn("Jar or folder does not exist: " + f.getAbsolutePath());
            }
            else if (f.isDirectory()){
                jars.addAll(FileUtils.listFiles(f,new String[]{"jar"},true));
            }
            else if (f.getName().endsWith(".jar")) {
                jars.add(f);
            }
        }

        for (File jar : jars) {
            if (!jar.exists()) {
                LOGGER.warn("Jar file does not exist: " + jar.getAbsolutePath());
            }
        }

        return jars;

    }

}
