package nz.ac.vuw.httpfuzz.commons;

import com.google.common.base.Preconditions;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Factory to support the creation of probabilistic value pairs using an internal DSL.
 * Delegate suppliers have to be registered with a probability (in percent), acc. to this probability
 * they are used. Probabilities have to add up to 100.
 * It is also possible to register some deterministic conditional delegate suppliers.
 * @author jens dietrich
 */
public class ProbabilisticSupplierFactory<V> {
    private SourceOfRandomness sourceOfRandomness = new SourceOfRandomness();

    private static enum State {INCOMPLETE,CONDITION_SET,PROBABILITY_SET, READY};
    private State state = State.INCOMPLETE;
    private int currentProbability = 0; // in percent
    private int sumOfProbabilities = 0; // in percent, must add up to 100
    private Supplier<Boolean> currentCondition = null;
    private Map<Supplier<V>,Integer> probabilisticDelegates = new LinkedHashMap<>();
    private Map<Supplier<V>,Supplier<Boolean>> conditionalDelegates = new LinkedHashMap<>();

    private ProbabilisticSupplierFactory(SourceOfRandomness sourceOfRandomness) {
        this.sourceOfRandomness = sourceOfRandomness;
    }
    public static <V> ProbabilisticSupplierFactory create(SourceOfRandomness sourceOfRandomness) {
        return new ProbabilisticSupplierFactory<V>(sourceOfRandomness);
    }

    private ProbabilisticSupplierFactory() {
    }

    public static <V> ProbabilisticSupplierFactory create() {
        return new ProbabilisticSupplierFactory<V>();
    }

    public ProbabilisticSupplierFactory onCondition(Supplier<Boolean> condition) {
        Preconditions.checkState(state==State.INCOMPLETE);
        Preconditions.checkState(this.currentCondition==null);
        this.currentCondition = condition;
        this.state = State.CONDITION_SET;
        return this;
    }

    public ProbabilisticSupplierFactory withProbability(int value) {
        Preconditions.checkArgument(value<=100,"value must be a percentage value between 0 and 100");
        Preconditions.checkArgument(value>=0,"value must be a percentage value between 0 and 100");
        Preconditions.checkState(sumOfProbabilities+value<=100,"sum of probabilities set cannot exceed 100");
        Preconditions.checkState(state==State.INCOMPLETE || this.sumOfProbabilities==100); // could add probability 0 after reaching 100
        this.currentProbability = value;
        this.sumOfProbabilities = this.sumOfProbabilities + value;
        this.state = State.PROBABILITY_SET;
        return this;
    }

    public ProbabilisticSupplierFactory createValue(Supplier<V> supplier) {
        Preconditions.checkState(state==State.PROBABILITY_SET || state==State.CONDITION_SET);
        if (state==State.PROBABILITY_SET) {
            this.probabilisticDelegates.put(supplier,currentProbability);
        }
        else if (state==State.CONDITION_SET) {
            this.conditionalDelegates.put(supplier,this.currentCondition);
            this.currentCondition = null;
        }
        state = this.sumOfProbabilities == 100 ? State.READY : State.INCOMPLETE;
        return this;
    }

    public Supplier<V> build() {
        Preconditions.checkState(state==State.READY);
        return () -> {
            // conditional delegates
            for (Supplier<V> delegate : conditionalDelegates.keySet()) {
                if (conditionalDelegates.get(delegate).get()) {
                    return delegate.get();
                }
            }

            // random delegates
            int random = sourceOfRandomness.nextInt(0, 100);
            Supplier<V> selection = null;
            int value = 0;
            for (Supplier<V> delegate : probabilisticDelegates.keySet()) {
                value = value + probabilisticDelegates.get(delegate);
                if (random <= value) {
                    selection = delegate;
                    break;
                }
            }
            assert selection != null;
            return selection.get();
        };
    }

}
