package test.nz.ac.vuw.httpfuzz.jee.spring;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.http.MethodSpec;
import nz.ac.vuw.httpfuzz.jee.war.EntryPointExtractor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.*;

public class EntryPointExtractionFromSpringTest {
    private File springWebApplication = null;
    private static EnumSet<StaticModel.Scope> SCOPES = EnumSet.of(StaticModel.Scope.APPLICATION);

    @Before
    public void setup() throws Exception {
        springWebApplication = new File("src/test/resources/springmvc/target/springmvc-1.0.0.jar");
        Preconditions.checkState(springWebApplication.exists(),"war " + springWebApplication.getAbsolutePath() + " does not exist, check whether test project in src/test/resources/springmvc/ has been built with \"mvn package\"");
    }

    @After
    public void tearDown () {
        springWebApplication = null;
    }

    @Test
    public void test1 () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(springWebApplication,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(3,entryPoints.size());
        EntryPoint entryPoint = entryPoints.stream().filter(ep -> ep.getUrlPattern().equals("ex/foos")).findAny().get();

        assertNotNull(entryPoint);
        assertEquals("ex/foos",entryPoint.getUrlPattern());
        assertEquals("example1.FooService",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertTrue(entryPoint.getVariablePathParts().isEmpty());
        assertTrue(entryPoint.getSupportedMethods().contains(MethodSpec.POST));

        String boundUrl = entryPoint.bindVariables((var,i) -> "foo");
        assertEquals("ex/foos",boundUrl);

    }

    @Test
    public void test2 () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(springWebApplication,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(3,entryPoints.size());
        EntryPoint entryPoint = entryPoints.stream().filter(ep -> ep.getUrlPattern().equals("ex/foos/{id}")).findAny().get();

        assertNotNull(entryPoint);
        assertEquals("ex/foos/{id}",entryPoint.getUrlPattern());
        assertEquals("example1.FooService",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertTrue(entryPoint.getSupportedMethods().contains(MethodSpec.GET));
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertEquals(1,entryPoint.getVariablePathParts().size());

        EntryPoint.VariablePathPart varPart1 = entryPoint.getVariablePathParts().get(0);
        assertEquals("{id}",varPart1.getToken());
        assertEquals("ex/foos/{id}",varPart1.getPath());
        assertEquals(8,varPart1.getPosition());

        assertEquals(String.class,varPart1.getType());
        // TODO type scanning not yet implemented, once done, this should be tested:
        // assertEquals(Long.class,varPart1.getType();

        String boundUrl1 = entryPoint.bindVariables((var,i) -> "42");
        assertEquals("ex/foos/42",boundUrl1);

        String boundUrl2 = entryPoint.bindVariables((var,i) -> "magic/42");
        assertEquals("ex/foos/magic/42",boundUrl2);

    }

    @Test
    public void test3 () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(springWebApplication,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(3,entryPoints.size());
        EntryPoint entryPoint = entryPoints.stream().filter(ep -> ep.getUrlPattern().equals("ex/foos/{fooid}/bar/{barid}")).findAny().get();

        assertNotNull(entryPoint);
        assertEquals("ex/foos/{fooid}/bar/{barid}",entryPoint.getUrlPattern());
        assertEquals("example1.FooService",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertTrue(entryPoint.getSupportedMethods().contains(MethodSpec.GET));
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertEquals(2,entryPoint.getVariablePathParts().size());

        EntryPoint.VariablePathPart varPart1 = entryPoint.getVariablePathParts().get(0);
        assertEquals("{fooid}",varPart1.getToken());
        assertEquals("ex/foos/{fooid}/bar/{barid}",varPart1.getPath());
        assertEquals(8,varPart1.getPosition());
        assertEquals(String.class,varPart1.getType());
        // TODO type scanning not yet implemented, once done, this should be tested:
        // assertEquals(Long.class,varPart1.getType();

        EntryPoint.VariablePathPart varPart2 = entryPoint.getVariablePathParts().get(1);
        assertEquals("{barid}",varPart2.getToken());
        assertEquals("ex/foos/{fooid}/bar/{barid}",varPart2.getPath());
        assertEquals(20,varPart2.getPosition());
        assertEquals(String.class,varPart2.getType());
        // TODO type scanning not yet implemented, once done, this should be tested:
        // assertEquals(Long.class,varPart2.getType();

        String boundUrl1 = entryPoint.bindVariables((var,i) -> "42");
        assertEquals("ex/foos/42/bar/42",boundUrl1);

        String boundUrl2 = entryPoint.bindVariables((var,i) -> {
            switch (i) {
                case 0 : return "42";
                case 1 : return "43";
                default : return "null";

            }
        });
        assertEquals("ex/foos/42/bar/43",boundUrl2);

    }

}
