package test.nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.http.ScalingUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScalingUtilTest {


    @Test
    public void test1() {
        int[] input = new int[]{1,2,3,4};
        int[] output = ScalingUtil.scaleTo100(input);
        print(output);
        assertTrue(Arrays.equals(new int[]{10,20,30,40},output));
    }

    @Test
    public void test2() {
        int[] input = new int[]{1,2,2};
        int[] output = ScalingUtil.scaleTo100(input);
        print(output);
        assertTrue(Arrays.equals(new int[]{20,40,40},output));
    }

    @Test
    public void test3() {
        int[] input = new int[]{1,2};
        int[] output = ScalingUtil.scaleTo100(input);
        print(output);
        assertTrue(Arrays.equals(new int[]{34,66},output));
    }

    @Test
    public void test4() {
        int[] input = new int[]{2,12,24};
        int[] output = ScalingUtil.scaleTo100(input);
        print(output);
        assertTrue(Arrays.equals(new int[]{6,31,63},output));
    }

    private void print(int[] arr) {
        String s = IntStream.of(arr).mapToObj(i -> ""+i).collect(Collectors.joining(" "));
        System.out.println(s);
    }
}
