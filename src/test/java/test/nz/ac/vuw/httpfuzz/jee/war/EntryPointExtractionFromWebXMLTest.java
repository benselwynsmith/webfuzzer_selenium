package test.nz.ac.vuw.httpfuzz.jee.war;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.http.MethodSpec;
import nz.ac.vuw.httpfuzz.jee.war.EntryPointExtractor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.*;

public class EntryPointExtractionFromWebXMLTest {
    private File war = null;
    private static EnumSet<StaticModel.Scope> SCOPES = EnumSet.of(StaticModel.Scope.APPLICATION);

    @Before
    public void setup() throws Exception {
        war = new File("src/test/resources/war-webxml1/target/war-webxml1-1.1.0.war");
        Preconditions.checkState(war.exists(),"war " + war.getAbsolutePath() + " does not exist, check whether test project in src/test/resources/war-webxml1/ has been built with \"mvn package\"");
    }

    @After
    public void tearDown () {
        war = null;
    }

    @Test
    public void testMainEntryPoint () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(war,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(2,entryPoints.size());
        EntryPoint entryPoint = entryPoints.stream().filter(ep -> ep.getUrlPattern().contains("main")).findAny().get();

        assertNotNull(entryPoint);
        assertEquals("main",entryPoint.getUrlPattern());
        assertEquals("example1.MainServlet",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertTrue(entryPoint.getVariablePathParts().isEmpty());
        assertEquals(MethodSpec.GET,entryPoint.getSupportedMethods().iterator().next());

    }

    @Test
    public void testFooEntryPointWithWildcards () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(war,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(2,entryPoints.size());
        EntryPoint entryPoint = entryPoints.stream().filter(ep -> ep.getUrlPattern().contains("foo")).findAny().get();

        assertNotNull(entryPoint);
        assertEquals("foo/*/bar/*",entryPoint.getUrlPattern());
        assertEquals("example1.FooServlet",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertEquals(2,entryPoint.getVariablePathParts().size());

        EntryPoint.VariablePathPart varPart1 = entryPoint.getVariablePathParts().get(0);
        assertEquals("*",varPart1.getToken());
        assertEquals("foo/*/bar/*",varPart1.getPath());
        assertEquals(4,varPart1.getPosition());
        assertEquals(String.class,varPart1.getType());

        EntryPoint.VariablePathPart varPart2 = entryPoint.getVariablePathParts().get(1);
        assertEquals("*",varPart2.getToken());
        assertEquals("foo/*/bar/*",varPart2.getPath());
        assertEquals(10,varPart2.getPosition());
        assertEquals(String.class,varPart2.getType());

        String boundUrl1 = entryPoint.bindVariables((var,i) -> "42");
        assertEquals("foo/42/bar/42",boundUrl1);

        String boundUrl2 = entryPoint.bindVariables((var,i) -> {
            switch (i) {
                case 0 : return "42";
                case 1 : return "43";
                default : return "null";

            }
        });
        assertEquals("foo/42/bar/43",boundUrl2);

    }

}
