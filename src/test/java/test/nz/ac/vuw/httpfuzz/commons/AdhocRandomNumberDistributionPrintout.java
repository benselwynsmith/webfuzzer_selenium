package test.nz.ac.vuw.httpfuzz.commons;

import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

/**
 * Adhioc script to check number distributions.
 */
public class AdhocRandomNumberDistributionPrintout {
    public static void main (String[] args) {
        SourceOfRandomness sor = new SourceOfRandomness();
        for (int i=0;i<100;i++) {
            int next = sor.getRandomPositiveNumberFromExponentialDistribution(1.3,5);
            System.out.println(next);
        }
    }
}
