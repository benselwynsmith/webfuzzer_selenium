package nz.ac.vuw.httpfuzz.jee.taint;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Data structure for tracking taint.
 * @author shawn
 */

public class Tainter {

    static ThreadLocal<List<String>> taintSourceStrings = new ThreadLocal<>();
    static ThreadLocal<Set<Object>> taintedObjects = new ThreadLocal<>();
    static ThreadLocal<Map<Object, Provenance>> taintProvenance = new ThreadLocal<>();
    static ThreadLocal<Boolean> trackingOn = new ThreadLocal<>();

    /**
    * Initialised by the filter with set of tainted string values
     * from http request header
     * a weak set of taintedObjects, initially empty
     * and a weakmap for provenance information for tainted objects
     * */
    static public void initialise(Set<String> taintedStrs) {
        taintSourceStrings.set(new ArrayList<>());
        taintSourceStrings.get().addAll(taintedStrs);

        taintedObjects.set(Collections.newSetFromMap(new WeakIdentityHashMap<>()));
        taintProvenance.set(new WeakIdentityHashMap<>());
    }

    static public void setTracking(boolean value) {
        if (taintSourceStrings.get() == null) {
           value = false;
        } else if (taintSourceStrings.get().isEmpty()) {
           value = false;
        }
        trackingOn.set(value);
    }

    static public boolean isTrackingOn() {
        if (trackingOn.get() != null) {
            return trackingOn.get();
        } else {
            return false;
        }
    }

    static public void clear() {
        taintSourceStrings.set(null);
        taintedObjects.set(null);
        taintProvenance.set(null);
    }


    /**
     * returns set of tainted arguments for the input call site
     * @param base receiver object for invocation
     * @param args array of arguments
     * @return list of tainted arguments
     */
    static public Set<Object> getTaintedArgsOfSink(Object base, Object[] args) {
        List<Object> argList = new ArrayList<>(Arrays.asList(args));
        argList.add(base);
        return argList.stream().filter(taintedObjects.get()::contains).collect(Collectors.toSet());
    }
    /**
     * returns list of encoded provenance objects for input set of tainted values
     * @param values set of tainted value arguments at call site
     * @param sinkName name of the target unsafe method at call site
     * @return list of encoded provenance strings
     */
    static public List<String> getProvenance(Set<Object> values, String sinkName) {
        List<Object> taintedValues = new ArrayList<>();
        Set<Object> processed = Collections.newSetFromMap(new IdentityHashMap<>());
        taintedValues.addAll(values);
        List<String> result = new ArrayList<>();

        while(!taintedValues.isEmpty()) {
            Object value = taintedValues.remove(0);
            if (processed.contains(value))
                continue;
            else {
                processed.add(value);
                Provenance p = taintProvenance.get().get(value);
                if (p != null) {
                    result.add(p.encode(values.contains(value), sinkName));
                    if (p.sourceType != TaintSource.SOURCE_STRING)
                        taintedValues.addAll(p.getSources());
                }
            }
        }
        return result;
    }

    /**
     * taint object if tainted value is stored in a field belonging to the object
     * @param base object into which field is stored
     * @param fieldName name of field
     * @param value the value into the base object
     */
    static public void taintByFieldStore(Object base, String fieldName, Object value) {
        if (taintedObjects.get().contains(value)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(value)),
                    TaintSource.FIELD_STORE, base, fieldName);
            taintProvenance.get().put(base, provenance);
            taintedObjects.get().add(base);
        }
    }

    /**
     * taint value loaded from field if object is tainted
     * @param base object from which field is loaded
     * @param fieldName name of field
     * @param value the value loaded from the base object
     */
    static public void taintByFieldLoad(Object base, String fieldName, Object value) {
        if (taintedObjects.get().contains(base)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(base)),
                    TaintSource.FIELD_LOAD, value, fieldName);
            taintProvenance.get().put(value, provenance);
            taintedObjects.get().add(value);
        }
    }

    /**
     * taint collection if added value is tainted
     * @param base collection
     * @param value the value added to the collection
     */
    static public void taintCollection(Object base, Object value) {
        if (taintedObjects.get().contains(value)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(value)),
                    TaintSource.COLLECTION_ADD, value, "collection add");
            taintProvenance.get().put(base, provenance);
            taintedObjects.get().add(base);
        }
    }

    /**
     * taint the return value of an invocation
     * @param sig signature of target method
     * @param base receiver object for invocation
     * @param args the arguments for the invocation
     * @param returnVal the value returned by the invocation
     */
    static public void taintByInvocation(String sig, Object base, Object[] args, Object returnVal) {

        if (returnVal == null)
            return;

        List<Object> argList = new ArrayList<>(Arrays.asList(args));
        argList.add(base);

        Set<Object> taintSources = (Collections.newSetFromMap(new WeakIdentityHashMap<>()));

        argList.stream()
                .filter(taintedObjects.get()::contains)
                .forEach(item -> taintSources.add(item));

        if (!taintedObjects.get().contains(returnVal)) {
            if (!taintSources.isEmpty()) {
                Provenance provenance = new Provenance(taintSources, TaintSource.ARG_TO_RETURN, returnVal, sig + " return");
                taintProvenance.get().put(returnVal, provenance);
                taintedObjects.get().add(returnVal);
                if (returnVal.getClass().isArray()) {
                    Object[] arr = (Object[]) returnVal;
                    for(int i = 0; i < arr.length; i++) {
                        taintProvenance.get().put(arr[i], provenance);
                        taintedObjects.get().add(arr[i]);
                    }
                }
            }
        }
    }

    /**
     *  introduce new taint if any arguments or the return value match the initial set of tainted string values read from
     *  the http request header: WEBFUZZ-TAINTED-VALUES
     *  these arguments/return value are added to the set of taintedObjects
     *  and provenance recorded in taintProvenance
     * @param sig signature of target method
     * @param base receiver object for invocation
     * @param args the arguments for the invocation
     * @param returnVal the value returned by the invocation
     */
    //
    public static void introduceNewTaint(String sig, Object base, Object[] args, Object returnVal) {
        if (returnVal != null && returnVal instanceof String) {
            if (taintSourceStrings.get().contains(returnVal)) {
                Object val = taintSourceStrings.get().get(taintSourceStrings.get().indexOf(returnVal)); // string from header to be set as source for this value (ref that won't be gc'ed)
                if (taintedObjects.get().add(returnVal)) {
                    Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(val)), TaintSource.SOURCE_STRING, returnVal, sig+" string return" );
                    taintProvenance.get().put(returnVal, provenance);
                }
            }
        }

        for(int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof String) {
                if (taintSourceStrings.get().contains(arg)) {
                    Object val = taintSourceStrings.get().get(taintSourceStrings.get().indexOf(arg));
                    if (taintedObjects.get().add(arg)) {
                        Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(val)), TaintSource.SOURCE_STRING, arg, sig + ", string arg:" + String.valueOf(i));
                        taintProvenance.get().put(arg, provenance);
                    }
                }
            }
        }
    }

}
