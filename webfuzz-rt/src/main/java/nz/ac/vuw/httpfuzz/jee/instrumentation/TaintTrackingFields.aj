package nz.ac.vuw.httpfuzz.jee.instrumentation;

import nz.ac.vuw.httpfuzz.jee.taint.Tainter;

/**
 * Aspect to track taint related to field stores and loads.
 * @author shawn
 */
public aspect TaintTrackingFields {



    before(Object value): set((!void && !byte && !short && !int && !long && !float && !double && !boolean && !char &&
            !byte[] && !short[] && !int[] && !long[] && !float[] && !double[] && !boolean[] && !char[]) *.*) && args(value) && !within(nz.ac.vuw.httpfuzz.jee..**) 
             && !cflow(execution(*.new(..))) && !within(groovy..*)
    {
       Object base = thisJoinPoint.getTarget();
       String fieldName = thisJoinPoint.toString();
        if (Tainter.isTrackingOn())
            Tainter.taintByFieldStore(base, fieldName, value);

    }


    after() returning(Object value): get((!void && !byte && !short && !int && !long && !float && !double && !boolean && !char &&
            !byte[] && !short[] && !int[] && !long[] && !float[] && !double[] && !boolean[] && !char[]) *.*)  && !within(nz.ac.vuw.httpfuzz.jee..**)
             && !cflow(execution(*.new(..))) && !within(groovy..*)
    {
        Object base = thisJoinPoint.getTarget();
        String fieldName = thisJoinPoint.toString();
        if (Tainter.isTrackingOn())
            Tainter.taintByFieldLoad(base, fieldName, value);

    }


}
