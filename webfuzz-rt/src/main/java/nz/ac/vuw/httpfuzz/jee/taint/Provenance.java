package nz.ac.vuw.httpfuzz.jee.taint;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Data structure for capturing  source of taint.
 * @author shawn
 */

public class Provenance {

    private Set<Object> sources;
    private WeakReference<Object> taintedValue;
    private String desc;
    Enum<TaintSource> sourceType;

    public Provenance(Set<Object> sources, Enum<TaintSource> sourceType, Object taintedValue, String desc) {
        this.sources = Collections.newSetFromMap(new WeakIdentityHashMap<>());
        this.sources.addAll(sources);
        this.desc = desc;
        this.taintedValue = new WeakReference<>(taintedValue);
        this.sourceType = sourceType;
    }
    /**
    * @return string formatted as: sources=<list of hash codes of sources>;type=<value's type>;hash=<value's hash>;desc=<taint type>;string=<value if it's a string>;sink=<unsafe method where value has flowed to>
    **/
    public String encode(boolean isSinkValue, String sinkName) {
        String encodedSources = this.sources.stream()
                .map( i -> { return String.valueOf(System.identityHashCode(i));})
                .collect(Collectors.joining(","));
        
        String str =  "sources=" + encodedSources + ";type=" + (taintedValue.get()!=null?taintedValue.get().getClass().getName():"n/a") + ";hash=" + System.identityHashCode(taintedValue.get()) + ";desc=" + desc;
        if (sourceType == TaintSource.SOURCE_STRING) {
            assert sources.iterator().hasNext();
            str += ";string="+sources.iterator().next();
        }
        if (isSinkValue) {
            str += ";sink=" + sinkName + "";
        } else {
            str += "";
        }
        return str;

    }

    public Set<Object> getSources() {
        return sources;
    }


}
