package test.nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.jee.rt.Constants;
import nz.ac.vuw.httpfuzz.jee.rt.InvocationTrackerManagerFilter;
import nz.ac.vuw.httpfuzz.jee.rt.TrackedInvocationsPickupServlet;
import nz.ac.vuw.httpfuzz.jee.taint.WeakIdentityHashMap;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.*;

import static org.junit.Assert.*;

public class ServletWithDataflowToSinkTest {


    @Test
    public void testDataflowTracking () throws Exception {

        // part 1: record invocations
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(Constants.TAINTED_INPUT_HEADER, "___FOO,length".toLowerCase());
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterConfig config = new MockFilterConfig();
        FilterChain chain = new MockFilterChain() {
            @Override
            public void doFilter(ServletRequest req, ServletResponse res) throws ServletException, IOException {
            new ServletWithDataflowToSink().doGet((HttpServletRequest)req,(HttpServletResponse)res);
            }
        };

        InvocationTrackerManagerFilter invocationTrackerManagerFilter = new InvocationTrackerManagerFilter();
        invocationTrackerManagerFilter.init(config);
        invocationTrackerManagerFilter.doFilter(request,response,chain);
        invocationTrackerManagerFilter.destroy();

        String ticket = response.getHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER);

        assertNotNull(ticket);

        // part 2: pick up recorded invocations
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        TrackedInvocationsPickupServlet pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);

        assertEquals("application/json",response.getContentType());

        JSONObject json = new JSONObject(response.getContentAsString());
        List<TaintFlowSpec> unsafeDataflows = Utils.extractTaintflows(json);
        System.out.println("= FLOWS TO UNSAFE METHODS =");
        assertFalse(unsafeDataflows.isEmpty());
        System.out.println("Sinks with dataflow:" + unsafeDataflows);
        assertTrue(unsafeDataflows.contains(new TaintFlowSpec("Runtime.exec(..)", Arrays.asList("___foo"), null)));
        assertTrue(unsafeDataflows.contains(new TaintFlowSpec("Method.invoke(..)", Arrays.asList("length"), null)));

    }

}
