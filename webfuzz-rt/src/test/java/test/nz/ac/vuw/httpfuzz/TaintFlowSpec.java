package test.nz.ac.vuw.httpfuzz;

import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * Representation for taintflow data from feedback
 * @author shawn
 */

public class TaintFlowSpec {
    public String sinkName;
    public List<String> sourceStrings;
    public String provenance;
    public TaintFlowSpec(String sinkName, List<String> sourceStrings, String provenance) {
        this.sinkName = sinkName;
        this.sourceStrings = sourceStrings;
        this.provenance = provenance;
    }
    public String toString() {
        return "TaintFlow{" + sinkName + ", " + sourceStrings + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintFlowSpec that = (TaintFlowSpec) o;
        return sinkName.equals(that.sinkName) &&
                sourceStrings.equals(that.sourceStrings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sinkName, sourceStrings);
    }
}
