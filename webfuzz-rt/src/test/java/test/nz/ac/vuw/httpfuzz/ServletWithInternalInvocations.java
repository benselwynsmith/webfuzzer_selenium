package test.nz.ac.vuw.httpfuzz;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletWithInternalInvocations extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        foo1();
        resp.setStatus(HttpServletResponse.SC_OK);
        if (req.getParameter("servlet-param0")!=null) {
            System.out.println("The magic parameter has been used");
        }
        if (req.getParameter("servlet-param1")!=null) {
            System.out.println("Another magic parameter has been used");
        }
    }

    private void foo1() {
        foo2();
    }

    private void foo2() {
    }
}
