package test.nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.jee.rt.DataKind;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Misc utils uses across tests.
 * @author jens dietrich
 * @author shawn
 * shawn - added methods for extracting taint flow
 */
public class Utils {

    static List<MethodSpec> extractInvocations(JSONObject json) {
        JSONArray invocationData = json.getJSONArray(DataKind.invokedMethods.name());
        List<MethodSpec> methods = new ArrayList<>(invocationData.length());
        for (int i=0;i<invocationData.length();i++) {
            String def = invocationData.getString(i);
            String[] parts = def.split("::");
            MethodSpec methodSpec = new MethodSpec(parts[0],parts[1],parts[2]);
            methods.add(methodSpec);
        }
        return methods;
    }

    static List<String> extractRequestParameterNames(JSONObject json) {
        JSONArray reqParamData = json.getJSONArray(DataKind.requestParameterNames.name());
        List<String> names = new ArrayList<>();
        for (int i=0;i<reqParamData.length();i++) {
            names.add(reqParamData.getString(i));
        }
        return names;
    }

    static List<String> extractStackTraces(JSONObject json) {
        JSONArray stackTraceData = json.getJSONArray(DataKind.unsafeSinkInvocationStackTraces.name());
        List<String> stackTraces = new ArrayList<>();
        for (int i=0;i<stackTraceData.length();i++) {
            stackTraces.add(stackTraceData.getString(i));
        }
        return stackTraces;
    }

     static List<TaintFlowSpec> extractTaintflows(JSONObject json) {
     JSONArray taintFlowsData = json.getJSONArray(DataKind.taintFlows.name());
     List<TaintFlowSpec> taintFlows = new ArrayList<>();
     for (int i=0; i < taintFlowsData.length(); i++) {
         taintFlows.add(extractTaintFlowSpec(taintFlowsData.getString(i)));
     }
     return taintFlows;
    }

    static TaintFlowSpec extractTaintFlowSpec(String s) {
        String sinkName = "";
        List<String> sourceStrings = new ArrayList<>();
        String[] p = s.split("\n");
        for(int i = 0; i < p.length; i++) {
            String item = p[i];
            String[] parts = item.split(";");
            for(int j = 0; j < parts.length; j++) {
                if (parts[j].startsWith("string")) {
                    sourceStrings.add(parts[j].substring(7));
                }
                if (parts[j].startsWith("sink")) {
                    sinkName = parts[j].substring(5);
                }
            }
        }
        return new TaintFlowSpec(sinkName, sourceStrings, s);
    }
}


