about derby2.war

basically the same as derby.war , but schema references in web.xml removed in order to prevent 
parser (within fuzzer) making network connections that fail on the server

in the parser API , validation is switched off, but the parsers anyway make attempts to access the DTD