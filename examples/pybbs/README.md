MYSQL
-----
Requires a MYSQL database to be running.
Uses root user with a blank password.
pybbs will create a self-named schema with required tables on startup if not present already.